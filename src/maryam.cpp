#include <iostream>
#include <filesystem>
#include <eisgenerator/translators.h>
#include <eisgenerator/eistype.h>

#include "csvBackend.h"
#include "common.h"

int main(int argc, char** argv)
{
	if(argc < 4)
	{
		std::cerr<<"Usage: "<<(argc > 0 ? argv[0] : "NULL")<<" [OUTDIR] [MODEL] [FILENAME(S)]\n";
		return 1;
	}

	std::filesystem::path outDir(argv[1]);
	if(!checkDir(outDir))
		return 1;

	try
	{
		for(int i = 3; i < argc; ++i)
		{
			try
			{
				io::CSVReader<3, io::trim_chars<' '>, io::no_quote_escape<'\t'>> csvFile(argv[i]);
				if(!csvFile.next_line())
					return 1;

				csvFile.read_header(io::ignore_extra_column, "Impedance Real part", "Impedance Imaginary Part", "Frequency [Hz]");

				std::string frequency;
				std::string real;
				std::string imaginary;
				ModelData data;
				data.modelStr = eis::relaxisToEis(argv[2]);
				data.id = std::to_string(i-3);
				while(csvFile.read_row(real, imaginary, frequency))
					data.data.push_back(eis::DataPoint(std::complex<fvalue>(std::stod(real), std::stod(imaginary)), std::stod(frequency)*2*M_PI));

				saveData("maryam", data, outDir, argv[i]);
			}
			catch(const io::error::missing_column_in_header& err)
			{
				std::cerr<<"Could not read file "<<err.what()<<std::endl;
			}
		}
	}
	catch (const std::invalid_argument& err)
	{
		std::cerr<<"Could not read file "<<err.what();
		return 1;
	}
	return 0;
}
