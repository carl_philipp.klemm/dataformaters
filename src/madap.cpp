#include <eisgenerator/translators.h>
#include <eisgenerator/eistype.h>
#include <iostream>
#include <string>
#include <vector>
#include <filesystem>

#include "csvBackend.h"
#include "tokenize.h"
#include "common.h"

void trimBrackets(std::string& str)
{
	if(str[0] == '[')
		str.erase(str.begin());
	if(str.back() == ']')
		str.pop_back();
}

std::vector<eis::DataPoint> parseEisDataFromMadapArrays(std::string& freqArray, std::string& realArray, std::string& imaginaryArray)
{
	assert(realArray.size() > 2);
	assert(imaginaryArray.size() > 2);
	trimBrackets(realArray);
	trimBrackets(imaginaryArray);
	trimBrackets(freqArray);

	std::vector<std::string> freqTokens = tokenize(freqArray, ',');
	std::vector<std::string> realTokens = tokenize(realArray, ',');
	std::vector<std::string> imaginaryTokens = tokenize(imaginaryArray, ',');
	assert(realTokens.size() == freqTokens.size() && realTokens.size() == imaginaryTokens.size());

	std::vector<eis::DataPoint> data;
	data.reserve(freqTokens.size());
	for(size_t i = 0; i < freqTokens.size(); ++i)
		data.push_back(eis::DataPoint(std::complex<fvalue>(std::stod(realTokens[i]), std::stod(imaginaryTokens[i])), 2*M_PI*std::stod(freqTokens[i])));

	std::sort(data.begin(), data.end(), [](const eis::DataPoint& a, const eis::DataPoint& b){return a.omega < b.omega;});
	return data;
}

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		std::cerr<<"Usage: "<<(argc > 0 ? argv[0] : "NULL")<<" [FILE] [OUTDIR]\n";
		return 1;
	}

	io::CSVReader<7, io::trim_chars<' ', '\t'>, io::no_quote_escape<';'>> csvFile(argv[1]);
	csvFile.read_header(io::ignore_extra_column, "experimentID", "temperature", "frequency",
		"real_impedance", "imaginary_impedance", "EIS_fittedParameters", "EIS_circuit");

	if(!csvFile.next_line())
		return 1;
	if(!csvFile.next_line())
		return 1;

	std::string frequencyArray;
	std::string realArray;
	std::string imaginaryArray;
	std::string paramArray;
	std::string circutString;
	std::string idStr;
	double temperature;

	std::filesystem::path outDir("./out");
	if(argc == 3)
		outDir = argv[2];

	if(!checkDir(outDir))
		return 1;

	while(csvFile.read_row(idStr, temperature, frequencyArray, realArray, imaginaryArray, paramArray, circutString))
	{
		ModelData data;
		data.modelStr = eis::madapToEis(circutString, paramArray);
		data.id = idStr;

		data.data = parseEisDataFromMadapArrays(frequencyArray, realArray, imaginaryArray);
		saveData("madap", data, outDir, argv[1]);
	}
}
