#include "common.h"

#include <eisgenerator/eistype.h>
#include <eisgenerator/translators.h>
#include <iostream>

bool saveData(const std::string& exportName, const ModelData& data, const std::filesystem::path& outDir, const std::filesystem::path& originFile)
{
	std::filesystem::path filename;
	std::string modelStrWithoutParams = data.modelStr;
	eis::purgeEisParamBrackets(modelStrWithoutParams);
	size_t classIndex = 0;

	do
	{
		filename.assign(exportName);
		filename.concat("_");
		filename.concat(modelStrWithoutParams);
		filename.concat("_");
		filename.concat(std::to_string(classIndex));
		filename.concat(".csv");
		++classIndex;
	} while(std::filesystem::exists(outDir/filename));

	if(!saveToDisk(eis::EisSpectra(data.data, data.modelStr, data.id + ", \"" + std::string(originFile.filename()) + "\""), outDir/filename))
	{
		std::cerr<<"Unable to save to "<<outDir/filename;
		return false;
	}
	return true;
}

bool checkDir(const std::filesystem::path& outDir)
{
	if(!std::filesystem::is_directory(outDir))
	{
		if(!std::filesystem::create_directory(outDir))
		{
			std::cerr<<outDir<<" dose not exist and can not be created\n";
			return false;
		}
	}
	return true;
}
