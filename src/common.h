#include <filesystem>
#include <vector>
#include <eisgenerator/eistype.h>

struct ModelData
{
	std::string modelStr;
	std::string id;
	std::vector<eis::DataPoint> data;
};

bool saveData(const std::string& exportName, const ModelData& data, const std::filesystem::path& outDir, const std::filesystem::path& originFile);
bool checkDir(const std::filesystem::path& outDir);
