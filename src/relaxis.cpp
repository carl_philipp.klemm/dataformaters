#include <cmath>
#include <eisgenerator/translators.h>
#include <eisgenerator/eistype.h>
#include <eisgenerator/model.h>
#include <relaxisloaderpp/relaxisloaderpp.h>
#include <iostream>
#include <string>
#include <vector>
#include <filesystem>
#include <set>

#include "common.h"

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		std::cerr<<"Usage: "<<(argc > 0 ? argv[0] : "NULL")<<" -o [OUTDIR] [FILE(s)]\n";
		return 1;
	}

	std::filesystem::path outDir("./out");

	std::set<int> ignoreArgs;
	for(int arg = 1; arg < argc; ++arg)
	{
		if(std::string(argv[arg]) == "-o")
		{
			ignoreArgs.insert(arg);
			if(arg+1 < argc)
			{
				outDir = argv[arg+1];
				ignoreArgs.insert(arg+1);
			}
		}
	}

	if(!checkDir(outDir))
		return 1;

	for(int arg = 1; arg < argc; ++arg)
	{
		if(ignoreArgs.find(arg) != ignoreArgs.end())
			continue;

		try
		{
			rlx::File file(argv[arg]);
			std::cout<<"Loaded "<<argv[arg]<<" file has "<<file.getProjectCount()<<" project(s)\n";

			for(size_t i = 0; i < file.getProjectCount(); ++i)
			{
				try
				{
					rlx::Project project = file.getProject(i);
					std::cout<<"Project "<<i<<" has "<<project.getSpectraCount()<<" spectra\n";
					for(size_t j = 0; j < project.getSpectraCount(); ++j)
					{
						try
						{
							rlx::Spectra spectra = project.getSpectra(j);
							if(!spectra.fitted)
							{
								std::cout<<"skipping spectra "<<spectra.id<<" as this spectra is not fitted\n";
								continue;
							}
							if(spectra.model.empty())
							{
								std::cout<<"skipping spectra "<<spectra.id<<" as this spectra has no model\n";
								continue;
							}

							std::cout<<spectra.model<<'\n';

							std::vector<fvalue> omega(spectra.data.size());
							for(size_t i = 0; i < spectra.data.size(); ++i)
								omega[i] = spectra.data[i].omega;

							eis::Model model(spectra.model);

							std::vector<eis::DataPoint> genData = model.executeSweep(omega);

							fvalue dist = eisNyquistDistance(spectra.data, genData);

							if(std::isnan(dist))
							{
								std::cout<<"skipping spectra "<<spectra.id<<" as this spectra dosent appear to be fitted correctly dist is NAN!\n";
								continue;
							}
							else if(std::isinf(dist) || dist > 2.0)
							{
								std::cout<<"skipping spectra "<<spectra.id<<" as this spectra dosent appear to be fitted correctly dist is too large\n";
								continue;
							}
							else
							{
								std::cout<<"spectra "<<dist<<" away from theoretical values\n";
							}

							ModelData data;
							data.data = spectra.data;
							data.modelStr = spectra.model;
							data.id = std::to_string(spectra.id);
							saveData("relaxis", data, outDir, argv[arg]);
						}
						catch(const eis::file_error& err)
						{
							std::cerr<<"error spectra " <<j<<" from file "<<argv[arg]<<" librelaxisloaderpp: "<<err.what()<<std::endl;
							continue;
						}
						catch(const eis::parse_errror& err)
						{
							std::cerr<<"error spectra " <<j<<" from file "<<argv[arg]<<' '<<err.what()<<std::endl;
							continue;
						}
					}
				}
				catch(const eis::file_error& err)
				{
					std::cerr<<"error reading file "<<argv[arg]<<" librelaxisloaderpp: "<<err.what()<<std::endl;
					continue;
				}
			}
		}
		catch(const eis::file_error& err)
		{
			std::cout<<"Cant load "<<argv[arg]<<' '<<err.what()<<std::endl;
			continue;
		}
	}
	return 0;
}
