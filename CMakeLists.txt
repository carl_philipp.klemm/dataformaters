cmake_minimum_required(VERSION 3.0)

project(dataformaters)

set(COMMON_SRC_FILES
	src/tokenize.cpp
	src/common.cpp
	)

set(COMMON_LINK_LIBRARIES -leisgenerator)

add_compile_options(
	"-Wall"
	"-Wno-reorder"
	"-Wfatal-errors"
	"-ffast-math"
	"-ftree-vectorize"
	"-g"
	"-fno-strict-aliasing"
	)

add_executable(madaptoeis ${COMMON_SRC_FILES} src/madap.cpp)
target_link_libraries(madaptoeis ${COMMON_LINK_LIBRARIES})
set_property(TARGET madaptoeis PROPERTY CXX_STANDARD 17)
install(TARGETS madaptoeis RUNTIME DESTINATION bin)

add_executable(relaxistoeis ${COMMON_SRC_FILES} src/relaxis.cpp)
target_link_libraries(relaxistoeis ${COMMON_LINK_LIBRARIES} -lrelaxisloaderpp)
set_property(TARGET relaxistoeis PROPERTY CXX_STANDARD 17)
install(TARGETS relaxistoeis RUNTIME DESTINATION bin)

add_executable(maryamtoeis ${COMMON_SRC_FILES} src/maryam.cpp)
target_link_libraries(maryamtoeis ${COMMON_LINK_LIBRARIES})
set_property(TARGET maryamtoeis PROPERTY CXX_STANDARD 17)
install(TARGETS maryamtoeis RUNTIME DESTINATION bin)

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -s")


